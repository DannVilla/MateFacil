﻿namespace CapaPresentacion.InterfacesProfesor
{
    partial class AgregarEjercicio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblRegistroAlumno = new System.Windows.Forms.Label();
            this.lblErrorGrupo = new System.Windows.Forms.Label();
            this.lblErrorGrupoAlumno = new System.Windows.Forms.Label();
            this.lblErrorNombreAlumno = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtGrupoAlumno = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.txtIdAlumno = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btmNuevoAlumno = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.textBox1);
            this.panel2.Controls.Add(this.lblRegistroAlumno);
            this.panel2.Controls.Add(this.lblErrorGrupo);
            this.panel2.Controls.Add(this.lblErrorGrupoAlumno);
            this.panel2.Controls.Add(this.lblErrorNombreAlumno);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.txtGrupoAlumno);
            this.panel2.Controls.Add(this.txtIdAlumno);
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.btmNuevoAlumno);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(469, 383);
            this.panel2.TabIndex = 8;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // lblRegistroAlumno
            // 
            this.lblRegistroAlumno.AutoSize = true;
            this.lblRegistroAlumno.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegistroAlumno.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblRegistroAlumno.Location = new System.Drawing.Point(180, 301);
            this.lblRegistroAlumno.Name = "lblRegistroAlumno";
            this.lblRegistroAlumno.Size = new System.Drawing.Size(45, 17);
            this.lblRegistroAlumno.TabIndex = 10;
            this.lblRegistroAlumno.Text = "label6";
            this.lblRegistroAlumno.Visible = false;
            // 
            // lblErrorGrupo
            // 
            this.lblErrorGrupo.AutoSize = true;
            this.lblErrorGrupo.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErrorGrupo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblErrorGrupo.Location = new System.Drawing.Point(228, 301);
            this.lblErrorGrupo.Name = "lblErrorGrupo";
            this.lblErrorGrupo.Size = new System.Drawing.Size(45, 17);
            this.lblErrorGrupo.TabIndex = 10;
            this.lblErrorGrupo.Text = "label6";
            this.lblErrorGrupo.Visible = false;
            // 
            // lblErrorGrupoAlumno
            // 
            this.lblErrorGrupoAlumno.AutoSize = true;
            this.lblErrorGrupoAlumno.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErrorGrupoAlumno.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lblErrorGrupoAlumno.Location = new System.Drawing.Point(23, 290);
            this.lblErrorGrupoAlumno.Name = "lblErrorGrupoAlumno";
            this.lblErrorGrupoAlumno.Size = new System.Drawing.Size(45, 17);
            this.lblErrorGrupoAlumno.TabIndex = 10;
            this.lblErrorGrupoAlumno.Text = "label6";
            this.lblErrorGrupoAlumno.Visible = false;
            // 
            // lblErrorNombreAlumno
            // 
            this.lblErrorNombreAlumno.AutoSize = true;
            this.lblErrorNombreAlumno.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErrorNombreAlumno.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lblErrorNombreAlumno.Location = new System.Drawing.Point(23, 194);
            this.lblErrorNombreAlumno.Name = "lblErrorNombreAlumno";
            this.lblErrorNombreAlumno.Size = new System.Drawing.Size(45, 17);
            this.lblErrorNombreAlumno.TabIndex = 10;
            this.lblErrorNombreAlumno.Text = "label6";
            this.lblErrorNombreAlumno.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label3.Location = new System.Drawing.Point(23, 232);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 17);
            this.label3.TabIndex = 9;
            this.label3.Text = "Respuesta";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label6.Location = new System.Drawing.Point(366, 40);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 17);
            this.label6.TabIndex = 9;
            this.label6.Text = "ID:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.label2.Location = new System.Drawing.Point(12, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 17);
            this.label2.TabIndex = 9;
            this.label2.Text = "Planteamiento";
            // 
            // txtGrupoAlumno
            // 
            this.txtGrupoAlumno.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtGrupoAlumno.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtGrupoAlumno.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtGrupoAlumno.HintForeColor = System.Drawing.Color.Empty;
            this.txtGrupoAlumno.HintText = "Respuesta";
            this.txtGrupoAlumno.isPassword = false;
            this.txtGrupoAlumno.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtGrupoAlumno.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtGrupoAlumno.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtGrupoAlumno.LineThickness = 3;
            this.txtGrupoAlumno.Location = new System.Drawing.Point(26, 253);
            this.txtGrupoAlumno.Margin = new System.Windows.Forms.Padding(4);
            this.txtGrupoAlumno.Name = "txtGrupoAlumno";
            this.txtGrupoAlumno.Size = new System.Drawing.Size(415, 33);
            this.txtGrupoAlumno.TabIndex = 8;
            this.txtGrupoAlumno.Text = "Grupo";
            this.txtGrupoAlumno.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // txtIdAlumno
            // 
            this.txtIdAlumno.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtIdAlumno.Enabled = false;
            this.txtIdAlumno.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtIdAlumno.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtIdAlumno.HintForeColor = System.Drawing.Color.Empty;
            this.txtIdAlumno.HintText = "";
            this.txtIdAlumno.isPassword = false;
            this.txtIdAlumno.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtIdAlumno.LineIdleColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtIdAlumno.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtIdAlumno.LineThickness = 3;
            this.txtIdAlumno.Location = new System.Drawing.Point(398, 32);
            this.txtIdAlumno.Margin = new System.Windows.Forms.Padding(4);
            this.txtIdAlumno.Name = "txtIdAlumno";
            this.txtIdAlumno.Size = new System.Drawing.Size(43, 25);
            this.txtIdAlumno.TabIndex = 8;
            this.txtIdAlumno.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(54)))), ((int)(((byte)(167)))));
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(467, 25);
            this.panel1.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Cursor = System.Windows.Forms.Cursors.Default;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(179, -1);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(133, 20);
            this.label5.TabIndex = 0;
            this.label5.Text = "Registro Ejercicio";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(425, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "X";
            // 
            // btmNuevoAlumno
            // 
            this.btmNuevoAlumno.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(54)))), ((int)(((byte)(167)))));
            this.btmNuevoAlumno.FlatAppearance.BorderSize = 0;
            this.btmNuevoAlumno.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.btmNuevoAlumno.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btmNuevoAlumno.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btmNuevoAlumno.ForeColor = System.Drawing.Color.White;
            this.btmNuevoAlumno.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btmNuevoAlumno.Location = new System.Drawing.Point(159, 330);
            this.btmNuevoAlumno.Name = "btmNuevoAlumno";
            this.btmNuevoAlumno.Size = new System.Drawing.Size(140, 40);
            this.btmNuevoAlumno.TabIndex = 6;
            this.btmNuevoAlumno.Text = "Guardar";
            this.btmNuevoAlumno.UseVisualStyleBackColor = false;
            this.btmNuevoAlumno.Click += new System.EventHandler(this.btmNuevoAlumno_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(15, 92);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(426, 99);
            this.textBox1.TabIndex = 11;
            // 
            // AgregarEjercicio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(469, 383);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AgregarEjercicio";
            this.Text = "Form2";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblRegistroAlumno;
        private System.Windows.Forms.Label lblErrorGrupo;
        private System.Windows.Forms.Label lblErrorGrupoAlumno;
        private System.Windows.Forms.Label lblErrorNombreAlumno;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        public Bunifu.Framework.UI.BunifuMaterialTextbox txtGrupoAlumno;
        public Bunifu.Framework.UI.BunifuMaterialTextbox txtIdAlumno;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Button btmNuevoAlumno;
        private System.Windows.Forms.TextBox textBox1;
    }
}